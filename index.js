const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const auth = require('./src/middleware/auth.js')
const app = express()
const port = 3005
const morgan = require('morgan'); // request logging middleware

app.use(morgan('dev')); 

let corsOption = {
    origin : "http://localhost:3005"
}

mongoose.connect('mongodb://localhost:27017/easy-notes',{
    useNewUrlParser:true
}).then(()=>{
    console.log('Database connected successfully...');
}).catch((err)=>{
    console.log(err);
    process.exit()
})


app.use(cors(corsOption))
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

// routes
require('./src/routes/Product.js')(app)
require('./src/routes/Auth.js')(app)

// route middleware
// app.use('/api',auth)

app.get('/', (req, res) => res.send('Hello World!'))
app.listen(port, () => console.log(`Example app listening on port ${port}!`))