const Product = require('../model/Product.js')
const verifyToken = require('../middleware/auth.js'); // Import the middleware

// create 
exports.create = (req,res) =>{

    if (!req.body.title || !req.body.price) 
    {    
        res.json({status:400,message:'Fill all content data....'})
    }

    const userId = req.body.id;

    const product = new Product({
        title:req.body.title,
        price:req.body.price,
        rating:req.body.rating,
        description:req.body.description,
        user: userId
    })

    product.save().then(data =>{
        res.json({status:200,sucess:true,message:'Product created successfully...',data})
    }).catch(err =>{
        res.json(err || {message:'Something went wrong!'});
    })
}

// all products
exports.products = (req,res) =>{
    Product.find()
    .then(data =>{
        res.json({status:true,message:'Product list...',data})
    }).catch(err=>{
        res.json( err||{status:false,message:'Something went wrong...'})
    })
}

//product
exports.product = (req,res) =>{
    Product.findOne({ _id: req.params.id }).populate({path: 'user', select: '-password'}).then(data =>{
        res.json({status:200,success:true,message:'Product...',data})
    }).catch(err=>{
        res.json( err||{status:500,success:false,message:'Something went wrong...'})
    })
}

//product by login user
exports.userProduct = async (req,res) =>{
    try {
        verifyToken(req, res, async () => {
            const userId = req.user;
            const products = await Product.find({ user: userId }).populate({path: 'user', select: '-password'});
            res.json({ status: 200, success: true, message: 'User products', data: products });
        });
    } catch (error) {
        res.status(500).json({ status: 500, success: false, message: 'Error fetching products', error: error.message });
    }
}

//update product
exports.update = (req,res) =>{
    Product.findByIdAndUpdate(req.params.id,{
        title:req.body.title,
        price:req.body.price,
        rating:req.body.rating,
        description:req.body.description
    },{new:true}).then(data =>{
        res.json({status:200,success:true,message:"Product update successfully...",data})
    }).catch(err =>{
        res.json( err||{staus:500,success:false,message:'Something went wrong!'})
    })
}

// delete product
exports.delete = async (req,res)=>{
    try {
    const product = await Product.findByIdAndDelete(req.params.id)
    if (product) {
        res.json({status:200,success:true,message:'Deleted successfully...'})
    }else {
        res.status(404).json({ message: 'Product not found' });
    }}
    catch (error) {
        res.status(500).json({ status: 500, success: false, message: 'Error fetching products', error: error.message });
    }
    
}