const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require('../model/Auth.js')
const verifyToken = require('../middleware/auth.js'); // Import the middleware


exports.register = async (req,res) =>{
    
    const existingUser = await User.findOne({ email: req.body.email });
    if (existingUser) {
        return res.status(400).json({ status: 400, success: false, message: 'Email already exists' });
    }
    
    const salt = await bcrypt.genSalt(10)   
    const hasPassword = await bcrypt.hash(req.body.password,salt)


    let user = new User({
        email:req.body.email,
        name:req.body.name,
        password:hasPassword,
        user_type_id:req.body.user_type_id
    })

    user.save((err,registeredUser) =>{
        if (err) {
                req.json(err||{status:400,success:false,message:'Something went wrong!'})
        } else {
            let registerUser = {
                id:registeredUser._id,user_type_id:req.body.user_type_id || 0
            }
            const token = jwt.sign(registerUser, "secret-Key")

            res.json({status:200,success:true,token})
        }
    })

}


exports.login = async (req, res) => { 
    const { email, password } = req.body;

    try {
        const user = await User.findOne({ email: email });

        if (user && await bcrypt.compare(password, user.password)) {
            const token = jwt.sign({ email, id: user._id.toString() }, "secret-Key");
            res.json({ message: 'Login successful', token });
        } else {
            res.status(401).json({ message: 'Invalid credentials' });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
}


exports.userlist = async (req, res) => { 
    try {
        // verifyToken(req, res, async () => {
            const userId = req.user;
            const users = await User.findById(userId).select('-password');
            res.json({ status: 200, message: "User List...", data: users });
        // });
    } catch (error) {
        res.status(500).json({ status: 500, message: 'Internal server error' });
    }
}