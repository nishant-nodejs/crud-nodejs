const verifyToken = require('../middleware/auth.js')

module.exports = (app) =>{
    const Auth = require('../controller/Auth.js')

    app.get('/userlist', verifyToken, Auth.userlist)
    app.post('/register', Auth.register)
    app.post('/login', Auth.login)
}