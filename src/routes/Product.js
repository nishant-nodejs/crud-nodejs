const product = require('../controller/Product.js')

module.exports = (app) =>{

    app.get('/products',product.products)
    app.get('/user/products',product.userProduct)
    app.post('/create-product',product.create)
    app.get('/product/:id',product.product)
    app.put('/update-product/:id',product.update)
    app.delete('/delete-product/:id',product.delete)

}