const jwt = require('jsonwebtoken');
const secretKey = "secret-Key"; // Replace with your actual secret key

function verifyToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    
    if (!authHeader) {
        return res.status(403).json({ message: 'Token not provided' });
    }
    const token = authHeader.split(' ')[1]; // Get the token part after "Bearer"

    jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
        return res.status(401).json({ message: 'Invalid token' });
        }
        req.user = decoded.id;
        next();
    });
}

module.exports = verifyToken;
