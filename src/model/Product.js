const mongoose = require('mongoose')

const product = mongoose.Schema({
    title:{
        type:String
    },
    price:{
        type:Number
    },
    rating:{
        type:Number
    },
    description:{
        type:String
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user", // Reference to the User model
        required: true // Make sure a user is associated with the product
    }
},{
    timestamps:true
})

module.exports = mongoose.model('Products',product)