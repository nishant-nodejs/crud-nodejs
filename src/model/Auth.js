const { default: mongoose } = require("mongoose");

const User = mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    unique: true
  },
  password: {
    type: String,
  },
  user_type_id: {
    type: Number,
  },
});

module.exports = mongoose.model("user", User);
